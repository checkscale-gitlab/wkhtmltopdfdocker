FROM debian:buster as prepare
WORKDIR /tmp
ADD https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_amd64.deb wkhtmltox.deb
RUN dpkg-deb -xv wkhtmltox.deb .


FROM debian:buster
RUN apt update
RUN apt install libxext6 libjpeg62-turbo libpng16-16 libxrender1 libfontconfig1 libssl1.1 xfonts-75dpi xfonts-base -y
COPY --from=prepare /tmp/usr/local/bin/wkhtmltopdf /bin/
